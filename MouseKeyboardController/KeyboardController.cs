﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace MouseKeyboardController
{
    public static class KeyboardController
    {
        public static void SequentialKeyInput(IEnumerable<System.Windows.Forms.Keys> keys)
        {
            int num = keys.Count() * 2;
            var inp = new Controller.INPUT[num];
            int inputIdx = 0;
            foreach (var key in keys)
            {
                // キーダウン
                inp[inputIdx].type = Controller.INPUT_KEYBOARD;
                inp[inputIdx].ki.wVk = (short)key;
                inp[inputIdx].ki.wScan = (short)Controller.MapVirtualKey(inp[inputIdx].ki.wVk, 0);
                inp[inputIdx].ki.dwFlags = Controller.KEYEVENTF_EXTENDEDKEY | Controller.KEYEVENTF_KEYDOWN;
                inp[inputIdx].ki.dwExtraInfo = 0;
                inp[inputIdx].ki.time = 0;
                inputIdx++;

                // キーアップ
                inp[inputIdx].type = Controller.INPUT_KEYBOARD;
                inp[inputIdx].ki.wVk = (short)key;
                inp[inputIdx].ki.wScan = (short)Controller.MapVirtualKey(inp[inputIdx].ki.wVk, 0);
                inp[inputIdx].ki.dwFlags = Controller.KEYEVENTF_EXTENDEDKEY | Controller.KEYEVENTF_KEYUP;
                inp[inputIdx].ki.dwExtraInfo = 0;
                inp[inputIdx].ki.time = 0;
                inputIdx++;
            }
            // キーボード操作実行
            Controller.SendInput(num, ref inp[0], Marshal.SizeOf(inp[0]));
            // 1000ミリ秒スリープ
            System.Threading.Thread.Sleep(1000);
        }

        public static void SimultaneousKeyInput(IEnumerable<System.Windows.Forms.Keys> keys)
        {
            int num = keys.Count() * 2;
            var inp = new Controller.INPUT[num];
            int inputIdx = 0;
            // キーダウン
            foreach (var key in keys)
            {
                inp[inputIdx].type = Controller.INPUT_KEYBOARD;
                inp[inputIdx].ki.wVk = (short)key;
                inp[inputIdx].ki.wScan = (short)Controller.MapVirtualKey(inp[inputIdx].ki.wVk, 0);
                inp[inputIdx].ki.dwFlags = Controller.KEYEVENTF_EXTENDEDKEY | Controller.KEYEVENTF_KEYDOWN;
                inp[inputIdx].ki.dwExtraInfo = 0;
                inp[inputIdx].ki.time = 0;
                inputIdx++;
            }
            // キーアップ
            foreach (var key in keys.Reverse())
            {
                inp[inputIdx].type = Controller.INPUT_KEYBOARD;
                inp[inputIdx].ki.wVk = (short)key;
                inp[inputIdx].ki.wScan = (short)Controller.MapVirtualKey(inp[inputIdx].ki.wVk, 0);
                inp[inputIdx].ki.dwFlags = Controller.KEYEVENTF_EXTENDEDKEY | Controller.KEYEVENTF_KEYUP;
                inp[inputIdx].ki.dwExtraInfo = 0;
                inp[inputIdx].ki.time = 0;
                inputIdx++;
            }
            // キーボード操作実行
            Controller.SendInput(num, ref inp[0], Marshal.SizeOf(inp[0]));
            // 1000ミリ秒スリープ
            System.Threading.Thread.Sleep(1000);
        }
        
    }
}
