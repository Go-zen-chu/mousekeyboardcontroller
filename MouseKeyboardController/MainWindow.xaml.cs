﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;

namespace MouseKeyboardController
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        Thread mousePositionThread;

        public MainWindow()
        {
            InitializeComponent();
            mousePositionThread = new Thread(new ThreadStart(() =>
            {
                while (true)
                {
                    Thread.Sleep(2000);
                    int x = System.Windows.Forms.Cursor.Position.X;
                    int y = System.Windows.Forms.Cursor.Position.Y;
                    mousePositionLabel.Dispatcher.BeginInvoke(new Action(()=>{
                        mousePositionLabel.Content = "(" + x + ", " + y + ")";
                    }));
                }
            }));
            mousePositionThread.Start();
        }

        #region app controller

        private void searchVisibleWindowsButton_Click(object sender, RoutedEventArgs e)
        {
            var processes = Controller.SearchVisibleWindowInfoAsync();
            processes.ContinueWith(task =>
            {
                windowTitleComboBox.Dispatcher.BeginInvoke(new Action(() =>
                {
                    windowTitleComboBox.ItemsSource = task.Result.Select(tpl => string.Join(", ", new object[] { tpl.Item1.Id, tpl.Item1.ProcessName, tpl.Item2 }));
                }));
            });
        }

        private void findByAppNameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && findByAppNameTextBox.Text != "")
            {
                findByAppNameResultLabel.Content = Controller.FindTitleByAppName(findByAppNameTextBox.Text);
            }
        }

        private void findByAppTitleTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && findByAppTitleTextBox.Text != "")
            {
                findByAppTitleResultLabel.Content = Controller.FindTitleByAppTitle(findByAppTitleTextBox.Text);
            }
        }
        
        #endregion

        #region mouse controller
        
        private void controlMouseButton_Click(object sender, RoutedEventArgs e)
        {
            var screenWidth = (int)SystemParameters.PrimaryScreenWidth;
            var screenHeight = (int)SystemParameters.PrimaryScreenHeight;
            int x, y;
            if(int.TryParse(mouseMoveToXTextBox.Text, out x) && int.TryParse(mouseMoveToYTextBox.Text, out y)){
                MouseController.MoveMouse(x, y, screenWidth, screenHeight);
            }
            if (leftButtonClickCheckBox.IsChecked.Value) MouseController.MouseLeftButtonClick();
            if (leftButtonDoubleClickCheckBox.IsChecked.Value) MouseController.MouseLeftButtonDoubleClick();
            if (rightButtonClickCheckBox.IsChecked.Value) MouseController.MouseRightButtonClick();
        }

        #endregion

        #region keyboard controller

        private void refreshWorkingProcessButton_Click(object sender, RoutedEventArgs e)
        {
            processesComboBox.ItemsSource = Process.GetProcesses().OrderBy(p => p.ProcessName);
            processesComboBox.DisplayMemberPath = "ProcessName";
        }

        private void appInputTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && appInputTextBox.Text != "")
            {
                var selectedProcess = processesComboBox.SelectedItem as Process;
                if (selectedProcess == null) return;
                Controller.SendTextToApp(selectedProcess, appInputTextBox.Text);
            }
        }
        #endregion

        #region KeyMouseRecorder
        
        private void selectMacroExportDirPathButton_Click(object sender, RoutedEventArgs e)
        {
            var bfd = new System.Windows.Forms.FolderBrowserDialog() { Description = "Select Dir for exporting macro" };
            if (bfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                macroExportDirPathTextBox.Text = bfd.SelectedPath;
                Properties.Settings.Default.MacroExportDirPath = bfd.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }

        private void selectMacroImportFilePathButton_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new System.Windows.Forms.OpenFileDialog() { Title = "Select macro file", DefaultExt = ".macro" };
            if (Properties.Settings.Default.MacroExportDirPath != "")
                ofd.FileName = Properties.Settings.Default.MacroExportDirPath;
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                macroImportFilePathTextBox.Text = ofd.FileName;
                Properties.Settings.Default.MacroImportFilePath = ofd.FileName;
                Properties.Settings.Default.Save();
            }
        }

        private void recordMacroToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            KeyMouseRecorder.RecordMacro(macroExportDirPathTextBox.Text);
        }

        private void MacroToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            KeyMouseRecorder.Stop();
        }

        private void playMacroToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            KeyMouseRecorder.PlayMacro(macroImportFilePathTextBox.Text);
        }
        #endregion

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (mousePositionThread != null)
            {
                mousePositionThread.Abort();
                mousePositionThread = null;
            }
        }



    }
}
