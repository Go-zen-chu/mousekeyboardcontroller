﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MouseKeyboardController
{
    public static class KeyMouseRecorder
    {
        public delegate int HookProc(int nCode, IntPtr wParam, IntPtr lParam);
        public const string DATETIME_FORMAT = "yyyyMMddHHmmssff";

        private static int journalHook;
        
        public const int WH_JOURNALRECORD = 0;
        public const int WH_JOURNALPLAYBACK = 1;
        public const int WH_GETMESSAGE = 3;
        public const int WM_CANCELJOURNAL = 0x004B;

        [StructLayout(LayoutKind.Sequential)]
        public class EventMsg
        {
            public int message;
            public int paramL;
            public int paramH;
            public int time;
            public int hwnd;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hInstance, int threadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnhookWindowsHookEx(int idHook);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int CallNextHookEx(int idHook, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet=CharSet.Auto)]
        public static extern IntPtr GetModuleHandle(string name);

        private static int lastTime;

        private const int HC_GETNEXT = 1;
        private const int HC_SKIP = 2;
        private const int HC_NOREMOVE = 3;
        private const int HC_ACTION = 0;

        private static List<EventMsg> macro = new List<EventMsg>();
        private static FileStream fStream;
        private static StreamWriter writer;
        private static StreamReader reader;
        private static HookProc JournalRecordProc;

        private static EventMsg currentValue;

        public static void RecordMacro(string exportDirPath)
        {
            if (Directory.Exists(exportDirPath) == false) return;
            fStream = new FileStream(Path.Combine(exportDirPath, DateTime.Now.ToString(DATETIME_FORMAT) + ".macro"), FileMode.Create, FileAccess.Write);
            writer = new StreamWriter(fStream);
            //IntPtr hinstance = GetModuleHandle(Process.GetCurrentProcess().MainModule.ModuleName);
            IntPtr hinstance = Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0]); ;
            JournalRecordProc = JournalRecordProcedure;
            journalHook = SetWindowsHookEx(WH_JOURNALRECORD, JournalRecordProc, hinstance, 0);
            var w = new Window() { Height = 100, Width = 100 };
            w.Content = journalHook;
            w.Show();
        }
        public static int JournalRecordProcedure(int nCode, IntPtr wParam, IntPtr lParam)
        {
            // if nCode < 0 you shouldn't handle
            if (nCode < 0) return CallNextHookEx(journalHook, nCode, wParam, lParam);

            var msg = (EventMsg)Marshal.PtrToStructure(lParam, typeof(EventMsg));
            //Write
            string json = ToJson(msg);
            writer.WriteLine(json);
            return CallNextHookEx(journalHook, nCode, wParam, lParam);
        }
        
        /// <summary>
        /// Unhook the window so it will stop both recording and playing
        /// </summary>
        public static void Stop()
        {
            UnhookWindowsHookEx(journalHook);
            journalHook = 0;
            if (writer != null) writer.Dispose();
            if (reader != null) reader.Dispose();
            if (fStream != null) fStream.Dispose();
        }
        
        
        public static void PlayMacro(string importFilePath)
        {
            if (File.Exists(importFilePath) == false) return;
            fStream = new FileStream(importFilePath, FileMode.Open, FileAccess.Read);
            reader = new StreamReader(fStream);
            string data = reader.ReadLine(); //buffer first item to get things rolling
            currentValue = FromJson<EventMsg>(data);

            IntPtr hinstance = Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0]);
            journalHook = SetWindowsHookEx(WH_JOURNALPLAYBACK, JournalPlaybackProc, hinstance, 0);
        }

        public static int JournalPlaybackProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            switch (nCode)
            {
                case HC_GETNEXT:
                    EventMsg msg = currentValue;
                    int delta = msg.time - lastTime;
                    Marshal.StructureToPtr(msg, lParam, true);
                    lastTime = msg.time;
                    return delta > 0 ? delta : 0;
                case HC_SKIP:
                    string data = reader.ReadLine();
                    if (data == null)
                    {
                        currentValue = null;
                        Stop();
                        return 0;
                    }
                    currentValue = FromJson<EventMsg>(data);
                    break;

                default:
                    return CallNextHookEx(journalHook, nCode, wParam, lParam);
            }
            return 0;
        }

        private static string ToJson<T>(T obj)
        {
            using (var stream = new MemoryStream())
            {
                var jsSerializer = new DataContractJsonSerializer(typeof(T));
                jsSerializer.WriteObject(stream, obj);

                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }

        private static T FromJson<T>(string input)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(input)))
            {
                var jsSerializer = new DataContractJsonSerializer(typeof(T));
                T obj = (T)jsSerializer.ReadObject(stream);
                return obj;
            }
        }
    }
}
