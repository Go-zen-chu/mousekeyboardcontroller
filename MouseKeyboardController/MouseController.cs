﻿using System.Runtime.InteropServices;

namespace MouseKeyboardController
{
    public static class MouseController
    {
        public static void MoveMouse(int x, int y, int screenWidth, int screenHeight)
        {
            int num = 1;
            Controller.INPUT[] inp = new Controller.INPUT[num];
            // マウスカーソルを移動する
            inp[0].type = Controller.INPUT_MOUSE;
            inp[0].mi.dwFlags = Controller.MOUSEEVENTF_MOVE | Controller.MOUSEEVENTF_ABSOLUTE;
            inp[0].mi.dx = x * (65535 / screenWidth);
            inp[0].mi.dy = y * (65535 / screenHeight);
            inp[0].mi.mouseData = 0;
            inp[0].mi.dwExtraInfo = 0;
            inp[0].mi.time = 0;

            // マウス操作実行
            Controller.SendInput(num, ref inp[0], Marshal.SizeOf(inp[0]));
            // 1000ミリ秒スリープ
            System.Threading.Thread.Sleep(1000);
        }

        public static void MouseRightButtonClick()
        {
            int num = 2;
            Controller.INPUT[] inp = new Controller.INPUT[num];
            // マウスの右ボタンを下げる
            inp[0].type = Controller.INPUT_MOUSE;
            inp[0].mi.dwFlags = Controller.MOUSEEVENTF_RIGHTDOWN;
            inp[0].mi.dx = 0;
            inp[0].mi.dy = 0;
            inp[0].mi.mouseData = 0;
            inp[0].mi.dwExtraInfo = 0;
            inp[0].mi.time = 0;
            // マウスの右ボタンを上げる
            inp[1].type = Controller.INPUT_MOUSE;
            inp[1].mi.dwFlags = Controller.MOUSEEVENTF_RIGHTUP;
            inp[1].mi.dx = 0;
            inp[1].mi.dy = 0;
            inp[1].mi.mouseData = 0;
            inp[1].mi.dwExtraInfo = 0;
            inp[1].mi.time = 0;

            // マウス操作実行
            Controller.SendInput(num, ref inp[0], Marshal.SizeOf(inp[0]));
            // 1000ミリ秒スリープ
            System.Threading.Thread.Sleep(1000);
        }

        public static void MouseLeftButtonClick()
        {
            int num = 2;
            Controller.INPUT[] inp = new Controller.INPUT[num];
            // マウスの左ボタンを下げる
            inp[0].type = Controller.INPUT_MOUSE;
            inp[0].mi.dwFlags = Controller.MOUSEEVENTF_LEFTDOWN;
            inp[0].mi.dx = 0;
            inp[0].mi.dy = 0;
            inp[0].mi.mouseData = 0;
            inp[0].mi.dwExtraInfo = 0;
            inp[0].mi.time = 0;
            // マウスの左ボタンを上げる
            inp[1].type = Controller.INPUT_MOUSE;
            inp[1].mi.dwFlags = Controller.MOUSEEVENTF_LEFTUP;
            inp[1].mi.dx = 0;
            inp[1].mi.dy = 0;
            inp[1].mi.mouseData = 0;
            inp[1].mi.dwExtraInfo = 0;
            inp[1].mi.time = 0;

            // マウス操作実行
            Controller.SendInput(num, ref inp[0], Marshal.SizeOf(inp[0]));
            // 1000ミリ秒スリープ
            System.Threading.Thread.Sleep(1000);
        }

        public static void MouseLeftButtonDoubleClick()
        {
            Controller.INPUT[] inp = new Controller.INPUT[1];
            // マウスの左ボタンを下げる
            inp[0].type = Controller.INPUT_MOUSE;
            inp[0].mi.dwFlags = Controller.MOUSEEVENTF_LEFTDOWN;
            inp[0].mi.dx = 0;
            inp[0].mi.dy = 0;
            inp[0].mi.mouseData = 0;
            inp[0].mi.dwExtraInfo = 0;
            inp[0].mi.time = 0;
            // マウスの左ボタンを上げる
            inp[1].type = Controller.INPUT_MOUSE;
            inp[1].mi.dwFlags = Controller.MOUSEEVENTF_LEFTUP;
            inp[1].mi.dx = 0;
            inp[1].mi.dy = 0;
            inp[1].mi.mouseData = 0;
            inp[1].mi.dwExtraInfo = 0;
            inp[1].mi.time = 0;

            // マウスの左ボタンを下げる
            inp[2].type = Controller.INPUT_MOUSE;
            inp[2].mi.dwFlags = Controller.MOUSEEVENTF_LEFTDOWN;
            inp[2].mi.dx = 0;
            inp[2].mi.dy = 0;
            inp[2].mi.mouseData = 0;
            inp[2].mi.dwExtraInfo = 0;
            inp[2].mi.time = 0;
            // マウスの左ボタンを上げる
            inp[3].type = Controller.INPUT_MOUSE;
            inp[3].mi.dwFlags = Controller.MOUSEEVENTF_LEFTUP;
            inp[3].mi.dx = 0;
            inp[3].mi.dy = 0;
            inp[3].mi.mouseData = 0;
            inp[3].mi.dwExtraInfo = 0;
            inp[3].mi.time = 0;

            // マウス操作実行
            Controller.SendInput(1, ref inp[0], Marshal.SizeOf(inp[0]));
            // 1000ミリ秒スリープ
            System.Threading.Thread.Sleep(1000);
        }

        public static void MouseWheel()
        {
            int num = 1;
            Controller.INPUT[] inp = new Controller.INPUT[num];
            // マウスホイールを前方(近づく方向)へ回転する
            inp[0].type = Controller.INPUT_MOUSE;
            inp[0].mi.dwFlags = Controller.MOUSEEVENTF_WHEEL;
            inp[0].mi.dx = 0;
            inp[0].mi.dy = 0;
            // +1に変更すれば遠ざかる方向に回転させる
            inp[0].mi.mouseData = -1 * Controller.WHEEL_DELTA;
            inp[0].mi.dwExtraInfo = 0;
            inp[0].mi.time = 0;

            // マウス操作実行
            Controller.SendInput(num, ref inp[0], Marshal.SizeOf(inp[0]));
            // 1000ミリ秒スリープ
            System.Threading.Thread.Sleep(1000);
        }
    }
}
