﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;

namespace MouseKeyboardController
{
    internal static class Controller
    {
        public const int INPUT_MOUSE = 0;                  // マウスイベント
        public const int INPUT_KEYBOARD = 1;               // キーボードイベント
        public const int INPUT_HARDWARE = 2;               // ハードウェアイベント

        public const int MOUSEEVENTF_MOVE = 0x1;           // マウスを移動する
        public const int MOUSEEVENTF_ABSOLUTE = 0x8000;    // 絶対座標指定
        public const int MOUSEEVENTF_LEFTDOWN = 0x2;       // 左　ボタンを押す
        public const int MOUSEEVENTF_LEFTUP = 0x4;         // 左　ボタンを離す
        public const int MOUSEEVENTF_RIGHTDOWN = 0x8;      // 右　ボタンを押す
        public const int MOUSEEVENTF_RIGHTUP = 0x10;       // 右　ボタンを離す
        public const int MOUSEEVENTF_MIDDLEDOWN = 0x20;    // 中央ボタンを押す
        public const int MOUSEEVENTF_MIDDLEUP = 0x40;      // 中央ボタンを離す
        public const int MOUSEEVENTF_WHEEL = 0x800;        // ホイールを回転する
        public const int WHEEL_DELTA = 120;                // ホイール回転値

        public const int KEYEVENTF_KEYDOWN = 0x0;          // キーを押す
        public const int KEYEVENTF_KEYUP = 0x2;            // キーを離す
        public const int KEYEVENTF_EXTENDEDKEY = 0x1;      // 拡張コード

        // マウスイベント(mouse_eventの引数と同様のデータ)
        [StructLayout(LayoutKind.Sequential)]
        public struct MOUSEINPUT
        {
            public int dx;
            public int dy;
            public int mouseData;
            public int dwFlags;
            public int time;
            public int dwExtraInfo;
        };

        // キーボードイベント(keybd_eventの引数と同様のデータ)
        [StructLayout(LayoutKind.Sequential)]
        public struct KEYBDINPUT
        {
            public short wVk;
            public short wScan;
            public int dwFlags;
            public int time;
            public int dwExtraInfo;
        };

        // ハードウェアイベント
        [StructLayout(LayoutKind.Sequential)]
        public struct HARDWAREINPUT
        {
            public int uMsg;
            public short wParamL;
            public short wParamH;
        };

        // 各種イベント(SendInputの引数データ)
        [StructLayout(LayoutKind.Explicit)]
        public struct INPUT
        {
            [FieldOffset(0)]
            public int type;
            [FieldOffset(4)]
            public MOUSEINPUT mi;
            [FieldOffset(4)]
            public KEYBDINPUT ki;
            [FieldOffset(4)]
            public HARDWAREINPUT hi;
        };

        // キー操作、マウス操作をシミュレート(擬似的に操作する)
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public extern static void SendInput(int nInputs, ref INPUT pInputs, int cbsize);

        // 仮想キーコードをスキャンコードに変換
        [DllImport("user32.dll", EntryPoint = "MapVirtualKeyA", CharSet = CharSet.Auto)]
        public extern static int MapVirtualKey(int wCode, int wMapType);
        
        // EnumWindowsから呼び出されるコールバック関数WNDENUMPROCのデリゲート
        public delegate bool WNDENUMPROC(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll")]
        public static extern bool EnumWindows(WNDENUMPROC lpEnumFunc, IntPtr lParam);

        [DllImport("user32.dll")]
        public static extern bool IsWindowVisible(IntPtr hWnd);

        // ウィンドウ名かアプリケーション名からそのIDを取得する
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        // ウィンドウに表示されているタイトルを取得する
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int cch);

        [DllImport("user32.dll")]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

         [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        private static List<Tuple<Process, string>> visibleProcessList;
        public static Task<List<Tuple<Process, string>>> SearchVisibleWindowInfoAsync()
        {
            if (visibleProcessList == null)
                visibleProcessList = new List<Tuple<Process, string>>();
            else
                visibleProcessList.Clear();
            return Task.Factory.StartNew(() =>
            {
                EnumWindows(EnumerateWindow, IntPtr.Zero);
                return visibleProcessList;
            });
        }
        // ウィンドウを列挙するためのコールバックメソッド
        private static bool EnumerateWindow(IntPtr hWnd, IntPtr lParam)
        {
            // ウィンドウが可視かどうか調べる
            if (IsWindowVisible(hWnd)){
                // ウィンドウのキャプションを取得・表示
                StringBuilder caption = new StringBuilder(0x1000);
                GetWindowText(hWnd, caption, caption.Capacity);
                int processId;
                GetWindowThreadProcessId(hWnd, out processId);
                // プロセスIDからProcessクラスのインスタンスを取得
                var p = Process.GetProcessById(processId);
                visibleProcessList.Add(new Tuple<Process, string>(p, caption.ToString()));
            }
            // ウィンドウの列挙を継続するにはtrueを返す必要がある
            return true;
        }

        public static string FindTitleByAppName(string appName)
        {
            //ウィンドウを探す
            IntPtr hWnd = FindWindow(appName, null);
            if (hWnd != null)
            {
                //ウィンドウタイトルを取得
                StringBuilder title = new StringBuilder(256);
                int titleLen = GetWindowText(hWnd, title, 256);
                return title.ToString();
            }
            else
                return "Can't find window";
        }
        public static string FindTitleByAppTitle(string appTitle)
        {
            //ウィンドウを探す
            IntPtr hWnd = FindWindow(null, appTitle);
            if (hWnd != null)
            {
                //ウィンドウタイトルを取得
                StringBuilder title = new StringBuilder(256);
                int titleLen = GetWindowText(hWnd, title, 256);
                return title.ToString();
            }
            else
                return "Can't find window";
        }

        public static void SendTextToApp(Process process, string text)
        {
            if (process == null && process.Responding == false) return;
            SetForegroundWindow(process.MainWindowHandle);
            SendKeys.SendWait(text);
        }
    }
}
